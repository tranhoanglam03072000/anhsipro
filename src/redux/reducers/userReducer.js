import { userInforLocal } from "../../service/local.service";
import { SET_LOGIN } from "../constants/userContants";

let initialState = {
  // khi user load trang
  userInfor: userInforLocal.get(),
};

export let userReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case SET_LOGIN: {
      state.userInfor = payload;
      return { ...state };
    }
    default:
      return state;
  }
};
