import axios from "axios";
import { userInforLocal } from "./local.service";
import { BASE_URL, configHeaders, https } from "./url.config";

// url = domain + endpoint
// request = header + body
export const movieService = {
  getBanner: () => {
    // return axios({
    //   url: `${BASE_URL}/api/QuanLyPhim/LayDanhSachBanner`,
    //   method: "GET",
    //   headers: configHeaders(),
    // });
    return https.get("/api/QuanLyPhim/LayDanhSachBanner");
  },
  getMovies: (params) => {
    // return axios({
    //   url: `${BASE_URL}/api/QuanLyPhim/LayDanhSachPhimPhanTrang`,
    //   method: "GET",
    //   headers: configHeaders(),
    //   params: params,
    // });
    let uri = "/api/QuanLyPhim/LayDanhSachPhimPhanTrang";
    return https.get(uri, params);
  },
  getMoviesByTheater: () => {
    // return axios({
    //   url: `${BASE_URL}/api/QuanLyRap/LayThongTinLichChieuHeThongRap`,
    //   method: "GET",
    //   headers: configHeaders(),
    // });
    let uri = "/api/QuanLyRap/LayThongTinLichChieuHeThongRap";
    return https.get(uri);
  },
};

// await api get userinfo => userId
// await api get history (userId)

//  api get movie list
//  api get user list
