import axios from "axios";

export const TOKEN_CYBERSOFT =
  "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0ZW5Mb3AiOiJCb290Y2FtcCDEkMOgIE7hurVuZyAwMyIsIkhldEhhblN0cmluZyI6IjAzLzAyLzIwMjMiLCJIZXRIYW5UaW1lIjoiMTY3NTM4MjQwMDAwMCIsIm5iZiI6MTY0NTgwODQwMCwiZXhwIjoxNjc1NTMwMDAwfQ.if3ZZ_VKK8nppxZJ2DF4FGoRPCmaYx6ncNAQytkjIT0";

export const BASE_URL = "https://movienew.cybersoft.edu.vn";

export const configHeaders = () => {
  return {
    TokenCybersoft: TOKEN_CYBERSOFT,
  };
};
export const https = axios.create({
  baseURL: BASE_URL,
  headers: configHeaders(),
});
