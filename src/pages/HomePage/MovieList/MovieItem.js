import { Button } from "antd";
import React from "react";

const MovieItem = (props) => {
  const { hinhAnh, tenPhim, moTa } = props.item;
  return (
    <div className="border border-black p-4 rounded-lg">
      <img alt="" src={hinhAnh} className="w-full h-60 object-cover" />
      <h2 className="text-2xl">{tenPhim}</h2>
      <p>{moTa.substr(0, 100) + "..."}</p>
      <Button>Đặt vé</Button>
    </div>
  );
};

export default MovieItem;
