import React, { useEffect, useState } from "react";
import { movieService } from "service/movie.service";
import MovieItem from "./MovieItem";

const MovieList = () => {
  // states =  [movieData, a]
  // states[1]

  const [movieData, setMovieData] = useState({});

  const fetchMovies = async ({ page = 1, pageSize = 8 }) => {
    const params = {
      maNhom: "GP01",
      soTrang: page,
      soPhanTuTrenTrang: pageSize,
    };

    try {
      const res = await movieService.getMovies(params);
      setMovieData(res.data.content);
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    fetchMovies({});
  }, []);

  // optional chaining
  return (
    <div className="container mx-auto">
      <h1>Danh sách phim</h1>
      <div className="grid grid-cols-4 gap-6 mt-10">
        {movieData.items?.map((item) => (
          <MovieItem key={item.maPhim} item={item} />
        ))}
      </div>
    </div>
  );
};

export default MovieList;

// React Element

// React component

// React component instance
