import React, { useEffect, useState } from "react";
import { Carousel } from "antd";
import { movieService } from "service/movie.service";

const HomeCarousel = () => {
  const [banners, setBanners] = useState([]);

  const fetchBanners = async () => {
    // call api => banners
    // async await
    try {
      const res = await movieService.getBanner();
      setBanners(res.data.content);
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    fetchBanners();
  }, []);

  return (
    <div>
      <Carousel>
        {banners.map((item) => {
          return (
            <div key={item.maBanner}>
              <img
                src={item.hinhAnh}
                alt=""
                className="h-100 object-cover w-full"
              />
            </div>
          );
        })}
      </Carousel>
    </div>
  );
};

export default HomeCarousel;
