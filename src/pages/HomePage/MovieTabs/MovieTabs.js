import { Tabs } from "antd";
import Spinner from "components/spiner/Spinner";
import React, { useState } from "react";
import { useEffect } from "react";
import { movieService } from "service/movie.service";
import MovieTabItem from "./MovieTabItem";

export default function MovieTabs() {
  let [dataRaw, setDataRaw] = useState([]);
  let [isLoading, setIsLoading] = useState(true);
  useEffect(() => {
    setIsLoading(true);
    movieService
      .getMoviesByTheater()
      .then((res) => {
        console.log(res);
        setDataRaw(res.data.content);
        setIsLoading(false);
      })
      .catch((err) => {
        console.log(err);
        setIsLoading(false);
      });
  }, []);

  let renderHeThongRap = () => {
    return dataRaw.map((heThongRap, index) => {
      console.log("cumRap: ", heThongRap);
      return (
        <Tabs.TabPane
          tab={<img className="w-20 h-20" src={heThongRap.logo} />}
          key={index}
        >
          <Tabs tabPosition="left" defaultActiveKey="1">
            {heThongRap.lstCumRap.map((cumRap) => {
              return (
                <Tabs.TabPane
                  key={cumRap.maCumRap}
                  tab={<div className="w-60">{cumRap.tenCumRap}</div>}
                >
                  {cumRap.danhSachPhim.map((phim) => {
                    return <MovieTabItem key={phim.maPhim} movie={phim} />;
                  })}
                </Tabs.TabPane>
              );
            })}
          </Tabs>
        </Tabs.TabPane>
      );
    });
  };
  return (
    <div className="pb-96 container mt-20">
      {isLoading && <Spinner />}
      <Tabs tabPosition="left" defaultActiveKey="1">
        {renderHeThongRap()}
      </Tabs>
    </div>
  );
}
