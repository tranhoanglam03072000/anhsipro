import moment from "moment";
import React from "react";

export default function MovieTabItem({ movie }) {
  return (
    <div className="flex p-3 items-center">
      <img className="h-32 w-24 object-cover" src={movie.hinhAnh} alt="" />
      <div className="ml-5 ">
        <p className="text-left text-xl font-medium">{movie.tenPhim}</p>
        <div className="grid grid-cols-3 gap-5">
          {movie.lstLichChieuTheoPhim.slice(0, 6).map((item) => {
            return (
              <span
                key={item.maLichChieu}
                className="p-3 bg-red-500 rounded text-white"
              >
                {moment(item.ngayChieuGioChieu).format("DD/YY - HH/MM")}
              </span>
            );
          })}
        </div>
      </div>
    </div>
  );
}
// {
//     "lstLichChieuTheoPhim": [
//         {
//             "maLichChieu": 44240,
//             "maRap": "723",
//             "tenRap": "Rạp 3",
//             "ngayChieuGioChieu": "2021-09-02T12:00:02",
//             "giaVe": 100000
//         }
//     ],
//     "maPhim": 1283,
//     "tenPhim": "Lat mat 48h1234",
//     "hinhAnh": "https://movienew.cybersoft.edu.vn/hinhanh/lat-mat-48h123_gp01.jpg",
//     "hot": true,
//     "dangChieu": true,
//     "sapChieu": true
// }
