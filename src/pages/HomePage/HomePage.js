import React from "react";
import Header from "../../components/Header/Header";
import HomeCarousel from "./Carousel/Carousel";
import MovieList from "./MovieList/MovieList";
import MovieTabs from "./MovieTabs/MovieTabs";

export default function HomePage() {
  return (
    <div>
      <Header />
      <HomeCarousel />
      <MovieList />
      <MovieTabs />
    </div>
  );
}
