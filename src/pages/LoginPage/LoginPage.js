import { Button, Form, Input, message } from "antd";
import React, { useEffect } from "react";
import { useNavigate } from "react-router-dom";
import { userInforLocal } from "../../service/local.service";
import { userServ } from "../../service/user.service";
import Lottie from "lottie-react";
import Bg_Animate from "../../assets/bg_login.json";
import { useDispatch } from "react-redux";
import { SET_LOGIN } from "../../redux/constants/userContants";
import { setLoginActionService } from "../../redux/actions/userAction";
export default function LoginPage() {
  let dispatch = useDispatch();

  let navigate = useNavigate();

  useEffect(() => {
    let userInfor = userInforLocal.get();

    if (userInfor) {
      navigate("/");
    }
    
    // kiểm tra nếu user đã đăng nhập thì chuyển hướng về trang chủ
  }, []);

  // const onFinish = (values) => {
  //   userServ
  //     .postLogin(values)
  //     .then((res) => {
  //       console.log(res);
  //       // lưu thông tin đăng nhập xuống localStorage
  //       userInforLocal.set(res.data.content);
  //       dispatch({
  //         type: SET_LOGIN,
  //         payload: res.data.content,
  //       });
  //       message.success("Đăng nhập thành công");
  //       setTimeout(() => {
  //         navigate("/");
  //       }, 1000);
  //     })
  //     .catch((err) => {
  //       message.error(err.response.data.content);
  //       console.log(err);
  //     });
  // };

  const onFinish = (values) => {
    let handleSuccess = () => {
      message.success("Đăng nhập thành công");
      setTimeout(() => {
        navigate("/");
      }, 500);
    };
    dispatch(setLoginActionService(values, handleSuccess));
  };
  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  return (
    <div className="h-screen w-screen bg-red-400 flex justify-center items-center px-20">
      <div className="container mx-auto p-5 bg-white rounded flex">
        <div className="w-1/2 transform scale-50">
          {/* animate */}
          <Lottie animationData={Bg_Animate} />
        </div>
        <div className="w-1/2 flex items-center justify-center">
          <Form
            className="w-full"
            layout="vertical"
            name="basic"
            labelCol={{
              span: 8,
            }}
            wrapperCol={{
              span: 24,
            }}
            initialValues={{
              remember: true,
            }}
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
            autoComplete="off"
          >
            <Form.Item
              label="Tài khoản"
              name="taiKhoan"
              rules={[
                {
                  required: true,
                  message: "Nhập thiếu rồi nha",
                },
              ]}
            >
              <Input />
            </Form.Item>

            <Form.Item
              label="Mật khẩu"
              name="matKhau"
              rules={[
                {
                  required: true,
                  message: "Nhập mật khẩu vô nha",
                },
              ]}
            >
              <Input.Password />
            </Form.Item>

            <Form.Item
              wrapperCol={{
                offset: 0,
                span: 24,
              }}
            >
              <Button
                className="bg-red-500 text-white border-none"
                htmlType="submit"
              >
                Submit
              </Button>
            </Form.Item>
          </Form>
        </div>
      </div>
    </div>
  );
}
