import React from "react";
import { ClimbingBoxLoader, PacmanLoader } from "react-spinners";

export default function Spinner() {
  return (
    <div className="fixed w-screen  h-screen flex justify-center items-center bg-black top-0 left-0">
      <PacmanLoader color="#7E22CD" margin={3} size={45} />
    </div>
  );
}
