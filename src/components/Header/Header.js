import React from "react";
import UserNav from "./UserNav";

export default function Header() {
  return (
    <div className="shadow-lg">
      <div className="container mx-auto flex justify-between items-center h-20">
        <span className="font-bold text-red-600 text-2xl">CyberFlix</span>

        <div className="space-x-5 font-medium">
          <span>Lịch Chiếu</span>
          <span>Cụm Rạp</span>
          <span>Tin Tức</span>s<span>Ứng Dụng</span>
        </div>

        <UserNav />
      </div>
    </div>
  );
}
