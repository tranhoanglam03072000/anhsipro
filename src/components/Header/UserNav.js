import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { NavLink, useNavigate } from "react-router-dom";
import { SET_LOGIN } from "../../redux/constants/userContants";
import { userInforLocal } from "../../service/local.service";

export default function UserNav() {
  let userInfor = useSelector((state) => {
    return state.userReducer.userInfor;
  });
  let dipatch = useDispatch();
  let navigate = useNavigate();

  let handleLogout = () => {
    // remove data local
    userInforLocal.remove();
    dipatch({
      type: SET_LOGIN,
      payload: null,
    });
    // chuyen trang
    // location.heft=> reload trang
    // window.location.href = "/";
    navigate("/");
  };

  let renderContent = () => {
    if (userInfor) {
      return (
        <div>
          <span className="mr-5 text-blue-500 font-medium text-lg">
            {userInfor.hoTen}
          </span>
          <button
            onClick={handleLogout}
            className="bg-red-500 text-white px-5 py-2 rounded shadow-lg"
          >
            Đăng xuất
          </button>
        </div>
      );
    } else {
      return (
        <div>
          <NavLink
            to="/login"
            className="bg-red-600 text-white px-5 py-2 rounded shadow-lg mr-5"
          >
            Đăng nhập
          </NavLink>
          <NavLink
            to="/register"
            className="bg-black text-white px-5 py-2 rounded shadow-lg"
          >
            Đăng kí
          </NavLink>
        </div>
      );
    }
  };
  console.log("userInfor: ", userInfor);
  return <div>{renderContent()}</div>;
}
